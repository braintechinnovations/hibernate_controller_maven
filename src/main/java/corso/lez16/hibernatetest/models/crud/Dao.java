package corso.lez16.hibernatetest.models.crud;

import java.util.List;

public interface Dao<T> {
	
	void insertion(T t);

	boolean deletion(T t);
	
	boolean deletion(int id);
	
	boolean update(T t);
	
	List<T> findAll();
	
	T findById(int id);
	
}

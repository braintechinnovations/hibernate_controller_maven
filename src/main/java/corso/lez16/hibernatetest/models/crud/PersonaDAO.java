package corso.lez16.hibernatetest.models.crud;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import corso.lez16.hibernatetest.models.Persona;
import corso.lez16.hibernatetest.models.db.GestoreSessioni;

public class PersonaDAO implements Dao<Persona> {

	public void insertion(Persona t) {

		SessionFactory factory = GestoreSessioni.getGestore().getFactory();
		Session sessione = factory.getCurrentSession();
		
		try {
			sessione.beginTransaction();			//1. Avvio una nuova transazione
			
			sessione.save(t);					//2. Effettuo tutte le operazioni che voglio
			
			sessione.getTransaction().commit();		//3. Se tutte le operazioni vanno a buon fine allora effettuo la Commit! Altrimenti, Rollback!
			
			System.out.println(t.toString());
			
		} catch (Exception errore) {
			System.out.println(errore.getMessage());
		} finally {
			sessione.close();						//Alla fine dell'utilizzo della sessione, chiudo!
			System.out.println("Connessione Chiusa!");
		}
		
	}

	public boolean deletion(Persona t) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean deletion(int id) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean update(Persona t) {
		// TODO Auto-generated method stub
		return false;
	}

	public List<Persona> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	public Persona findById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

}

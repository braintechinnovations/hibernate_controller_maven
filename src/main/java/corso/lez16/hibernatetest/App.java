package corso.lez16.hibernatetest;

import java.util.Scanner;

import corso.lez16.hibernatetest.controllers.PersonaController;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	
    	PersonaController contrPer = new PersonaController();
    	
//    	if(contrPer.inserisciPersona("Giorgetto", "Giorgetti", "giorgetto@ciao.com")) {
//    		System.out.println("Inserimento eseguito con successo");
//    	}
//    	else {
//    		System.out.println("Errore di inserimento");
//    	}
    	
    	Scanner interceptor = new Scanner(System.in);
    	boolean inserimentoAbilitato = true;
    	
    	while(inserimentoAbilitato) {
    		
    		System.out.println("Digita I per inserimento, Q per uscita:");
    		String input = interceptor.nextLine();
    		
    		switch (input) {
			case "I":
				System.out.println("Nome:");
				String nome = interceptor.nextLine();
				System.out.println("Cognome:");
				String cognome = interceptor.nextLine();
				System.out.println("Email:");
				String email = interceptor.nextLine();
				
				if(contrPer.inserisciPersona(nome, cognome, email)) {
					System.out.println("Inserimento eseguito con successo!");
				}
				else {
					System.out.println("Errore di inserimento ;(");
				}
				break;
			case "Q":
				inserimentoAbilitato = !inserimentoAbilitato;			//Toggle
				break;
			default:
				System.out.println("Comando non riconosciuto");
				break;
			}
    		
    	}
    	
    	
    }
}

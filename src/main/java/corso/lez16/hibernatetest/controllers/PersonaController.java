package corso.lez16.hibernatetest.controllers;

import corso.lez16.hibernatetest.models.Persona;
import corso.lez16.hibernatetest.models.crud.PersonaDAO;

public class PersonaController {

	public boolean inserisciPersona(String varNome, String varCognome, String varEmail) {
		
		PersonaDAO perDao = new PersonaDAO();
		
		Persona temp = new Persona();
		temp.setNome(varNome);
		temp.setCognome(varCognome);
		temp.setEmail(varEmail);
		
		perDao.insertion(temp);
		
		if(temp.getId() > 0) {
			return true;
		}
		else {
			return false;
		}
		
	}
	
}
